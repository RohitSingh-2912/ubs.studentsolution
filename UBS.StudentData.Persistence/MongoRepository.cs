﻿using MongoDB.Driver;
using UBS.StudentData.Abstraction;
using System.Threading.Tasks;
using MongoDB.Bson;
using UBS.StudentData.Abstraction.Models;

namespace UBS.StudentData.Persistence
{
    public class MongoRepository<TEntity> : IRepository<TEntity> where TEntity : IEntity
    {
        private readonly IMongoClient _client = null;
        private readonly IMongoDatabase _database = null;
        private readonly IMongoCollection<TEntity> _collection = null;

        public MongoRepository(IDatabaseSettings settings)
        {
         

          _client = new MongoClient(settings.ConnectionString);
          _database = _client.GetDatabase(settings.DatabaseName);
          _collection = _database.GetCollection<TEntity>(typeof(TEntity).Name);
        }

        public async Task<TEntity> Get(string id)
        {
            var entity = await _collection.Find(i => i.Id == id).FirstOrDefaultAsync();
            return entity;
        }

        public async Task<TEntity> Store(TEntity entity)
        {
           // entity.Id = ObjectId.GenerateNewId().ToString();
            await _collection.InsertOneAsync(entity);
            return entity;
        }

        public async Task<TEntity> Replace(TEntity entity)
        {
            await _collection.ReplaceOneAsync(e => e.Id == entity.Id, entity);
            return entity;
        }

        public async Task<long> Delete(string id)
        {
            var deleteResult = await _collection.DeleteOneAsync(e => e.Id == id);
            return deleteResult.DeletedCount;
        }



    }

}
