//1.
var http = require('http');
 

console.log("Doing the DELETE Operations...."); 


 
//2
var extServerOptionsPut = {
    host: 'localhost',
    port: '5000',
    path: '/api/student/108',
    method: 'DELETE'
  
};
 
 
//3
var reqDelete = http.request(extServerOptionsPut, function (res) {
    console.log("response statusCode: ", res.statusCode);
    res.on('data', function (data) {
        console.log('Deleting Result:\n');
        process.stdout.write(res);
        console.log('\n\nDELETE Operation Completed');
    });
});
 
// 7

reqDelete.end();
reqDelete.on('error', function (e) {
    console.error(e);
});
 
