//1.
var http = require('http');
 
var stud = [];
 
//2.
var extServerOptions = {
    host: 'localhost',
    port: '5000',
    path: '/api/student',
    method: 'GET'
};
//3.
function get() {
    http.request(extServerOptions, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (data) {
            stud = JSON.parse(data);
            stud.foreach(function (s) {
                console.log(s.Id+ "\t" + s.Name + "\t" + s.Age + "\t" + s.Gneder);
            });
        });
 
    }).end();
};
 
get();


console.log("Doing the PUT Operations...."); 



//4
var student = JSON.stringify({
	'Id':'107',
    'Name': 'Manoj',
    'Age': 28,
    'Gender': 'Male'
    
});
 
 
//5
var extServerOptionsPut = {
    host: 'localhost',
    port: '5000',
    path: '/api/student',
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json',
        'Content-Length': student.length
    }
};
 
 
//6
var reqPut = http.request(extServerOptionsPut, function (res) {
    console.log("response statusCode: ", res.statusCode);
    res.on('data', function (data) {
        console.log('Puting Result:\n');
        process.stdout.write(data);
        console.log('\n\nPUT Operation Completed');
    });
});
 
// 7
reqPut.write(student);
reqPut.end();
reqPut.on('error', function (e) {
    console.error(e);
});
 
get();