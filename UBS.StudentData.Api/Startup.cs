﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UBS.StudentData.Abstraction;
using UBS.StudentData.Abstraction.Models;
using UBS.StudentData.Persistence;
using UBS.StudentData.Services;

namespace UBS.StudentData.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddTransient<IDatabaseSettings>(p =>
            {
                return new DatabaseSettings
                {
                    DatabaseName = Environment.GetEnvironmentVariable("DATABASE_NAME"),
                    ConnectionString = Environment.GetEnvironmentVariable("CONNECTION_STRING")
                };
            });

            services.AddTransient< IRepository<Student>, MongoRepository<Student> >();
            services.AddTransient<StudentService>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
