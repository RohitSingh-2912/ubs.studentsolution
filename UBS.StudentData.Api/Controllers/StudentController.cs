﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UBS.StudentData.Abstraction.Models;
using UBS.StudentData.Services;

namespace UBS.StudentData.Api.Controllers
{
    [Route("api/Student")]
    [ApiController]
    public class StudentController : Controller
    {
       
            public StudentController(StudentService studentServices)
            {
               StudentService = studentServices ?? throw new ArgumentNullException(nameof(studentServices));
            }


            public StudentService StudentService { get; }


            [HttpGet]
            [Route("{id}")]
            public async Task<ActionResult> GetById([FromRoute] string Id)
            {
                return Ok(await StudentService.Get(Id));
            }

            [HttpPost]
            public async Task<IActionResult> Insert([FromBody]Student student)
            {
                return Ok(await StudentService.Insert(student));
            }

            [HttpPut]
            public async Task<ActionResult> Update([FromBody]Student student)
            {
                await StudentService.Update(student);
                return Ok(student);

            }

            [HttpDelete]
            [Route("{id}")]
            public async Task<IActionResult> Delete([FromRoute]string id)
            {
                return Ok(await StudentService.Delete(id));
            }
    
     }



}