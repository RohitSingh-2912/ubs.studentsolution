﻿using System;
using System.Threading.Tasks;
using UBS.StudentData.Abstraction;
using UBS.StudentData.Abstraction.Models;

namespace UBS.StudentData.Services
{
    public class StudentService : IStudentService
    {
        private readonly IRepository<Student> repo = null;


        public StudentService(IRepository<Student> repo)
        {
            this.repo = repo ?? throw new ArgumentNullException(nameof(repo));
        }

        public async Task<Student> Get(string id)
        {
            return await repo.Get(id);
        }

        public async Task<Student> Insert(Student student)
        {

            await repo.Store(student);
            return student;
        }

        public async Task<Student> Update(Student student)
        {
            await repo.Replace(student);
            return student;

        }

        public async Task<long> Delete(string id)
        {
            return await repo.Delete(id);
        }

     
    }
}
