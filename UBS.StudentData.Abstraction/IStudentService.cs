﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UBS.StudentData.Abstraction.Models;

namespace UBS.StudentData.Abstraction
{
    public interface IStudentService 
    {
        Task<Student> Get(string id);
        Task<Student> Insert(Student student);
        Task<Student> Update(Student student);
        Task<long> Delete(string id);
    }
}
