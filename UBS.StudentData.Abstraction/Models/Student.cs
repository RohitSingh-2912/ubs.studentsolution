﻿using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace UBS.StudentData.Abstraction.Models
{
    public class Student:IEntity
    {
        [BsonId]
        [Required]
        public string Id { get; set; }

        [Required(ErrorMessage = "Name is Required")]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Age is Required")]
        [Range(1, 100, ErrorMessage = "Age must be between 1-100 in years.")]
        public int Age { get; set; }

        [Required(ErrorMessage = "Gender is Required")]
        [StringLength(50, MinimumLength = 4)]
        public string Gender { get; set; }

    }
}
