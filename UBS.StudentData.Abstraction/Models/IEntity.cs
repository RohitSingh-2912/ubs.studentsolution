﻿

namespace UBS.StudentData.Abstraction.Models
{
   public interface IEntity
    {
        string Id { get; set; }
    }
}
