﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UBS.StudentData.Abstraction.Models;

namespace UBS.StudentData.Abstraction
{
    public interface IRepository<TEntity> where TEntity : IEntity
    {
        Task<long> Delete(string id);
        Task<TEntity> Get(string id);
        Task<TEntity> Replace(TEntity entity);
        Task<TEntity> Store(TEntity entity);
    }
}
