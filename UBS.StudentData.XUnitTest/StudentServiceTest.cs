using Moq;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using UBS.StudentData.Abstraction;
using UBS.StudentData.Abstraction.Models;
using UBS.StudentData.Services;
using Xunit;

namespace UBS.StudentData.XUnitTest
{
    public class StudentServiceTest
    {
        private readonly IStudentService studentService;
        private readonly Mock<IRepository<Student>> mockedRepository;
        public StudentServiceTest()
        {
            mockedRepository = new Mock<IRepository<Student>>();
            studentService = new StudentService(mockedRepository.Object);

        }
        [Fact]
        public async Task GetById()
        {
            //Arrange
            var student = new Student
            {
                Id = "105",
                Name = "Shruti",
                Age = 29,
                Gender = "Female"

            };

            mockedRepository
            .Setup(repo => repo.Get(It.Is<string>(id => id == "105")))
            .Returns(Task.FromResult < Student >(student));
            //act
            var result = await studentService.Get("105");

            //assert
            Assert.Equal(JsonConvert.SerializeObject(student), JsonConvert.SerializeObject(result));
        }

        [Fact]
        public async Task Store()
        {
            //Arrange
            var student = new Student
            {

                Id = "106",
                Name = "Rohit",
                Age = 29,
                Gender = "Male"

            };

            mockedRepository
           .Setup(repo => repo.Store(student))
           .Returns(Task.FromResult<Student>(student));

            //Act
            var result = await studentService.Insert(student);

            //Assert
            Assert.Equal(JsonConvert.SerializeObject(student), JsonConvert.SerializeObject(result));

        }

        [Fact]
        public async Task Replace()
        {

            //Arrange
            var student = new Student
            {

                Id = "104",
                Name = "Rohit",
                Age = 29,
                Gender = "Male"

            };

            mockedRepository
         .Setup(repo => repo.Replace(student))
         .Returns(Task.FromResult<Student>(student));

            //Act
            var result = await studentService.Update(student);

            //Assert
            Assert.Equal(JsonConvert.SerializeObject(result), JsonConvert.SerializeObject(student));



        }

        [Fact]
        public async Task Delete()
        {
            //Arrange
            var student = new Student
            {
                Id = "105",
                Name = "Shruti",
                Age = 29,
                Gender = "Female"

            };

            mockedRepository
            .Setup(repo => repo.Delete(It.Is<string>(id => id == "105")))
            .Returns(Task.FromResult<long>(1));
            //act
            var result = await studentService.Delete("105");

            //assert
            Assert.Equal(JsonConvert.SerializeObject(result), JsonConvert.SerializeObject(1));

        }
    }
}
